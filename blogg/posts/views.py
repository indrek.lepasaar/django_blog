from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Posts, FeaturedPost
from .forms import CommentForm, PostForm
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from rest_framework import viewsets
from .serializers import PostsSerializer, FeaturedPostsSerializer
from rest_framework import generics

# def index_page(request):
#     return render(request, 'home.html')
#


class ViewFeaturedPostPermissionMixin(PermissionRequiredMixin):
    permission_required = 'posts.view.featured_posts'

    def has_permission(self):
        return self.request.user.has_perm(self.permission_required)


def home(request):
    # Sample blog data
    posts = Posts.objects.all()
    posts = Posts.objects.order_by('-date_posted')
    featured_posts = FeaturedPost.objects.all().order_by('priority')
    context = {'posts': posts, 'featured_posts': featured_posts}
    return render(request, 'home.html', context)


class HomeView(ListView, ViewFeaturedPostPermissionMixin):
    model = Posts
    template_name = 'home.html'
    context_object_name = 'posts'
    ordering = ['title']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.has_permission():
            context['featured_posts'] = FeaturedPost.objects.all().order_by('priority')
        visits_count = self.request.session.get('visits_count', 0)
        self.request.session['visits_count'] = visits_count + 1
        context['count'] = visits_count
        return context


# functional based view

def post_detail(request, pk):
    # Retrieve the Post object
    post = get_object_or_404(Posts, pk=pk)

    # Handle the POST request
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.author = request.user
            comment.post = post
            comment.save()
            return redirect('post_detail', pk=post.pk)
    else:
        comment_form = CommentForm()

    # Context data for rendering the template
    context = {
        'post': post,
        'comments': post.comments.all(),
        'comment_form': comment_form
    }

    return render(request, 'posts/post_detail.html', context)


class PostCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Posts
    form_class = PostForm
    template_name = 'posts/post_form.html'
    success_url = reverse_lazy('home')
    permission_required = 'posts.add_posts'


class PostUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Posts
    form_class = PostForm
    template_name = 'posts/post_form.html'
    success_url = reverse_lazy('home')
    permission_required = 'posts.change_posts'


class PostDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Posts
    template_name = 'posts/post_confirm_delete.html'
    success_url = reverse_lazy('home')
    permission_required = 'posts.delete_posts'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        post = self.get_object()
        context['post'] = post
        return context

# class based view
# class PostDetailView(DetailView):
#     model = Posts
#     template_name = 'posts/post_detail.html'
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['comments'] = self.object.comments.all()
#         return context
#
#     def post(self, request, *args, **kwargs):
#         self.object = self.get_object()
#         comment_form = CommentForm(request.POST)
#         if comment_form.is_valid():
#             comment = comment_form.save(commit=False)
#             comment.author = request.user
#             comment.post = self.object
#             comment.save()
#             return redirect('post_detail', pk=self.object.id)
#         context = self.get_context_data(object=self.object)
#         context['comment_form'] = comment_form
#         return self.render_to_response(context)




# ************************** REST API VIEWS ******************

class PostViewSet(viewsets.ModelViewSet):
    queryset = Posts.objects.all()
    serializer_class = PostsSerializer

class FeaturePostViewSet(viewsets.ModelViewSet):
    queryset = FeaturedPost.objects.all()
    serializer_class = PostsSerializer
