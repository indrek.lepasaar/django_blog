from django.urls import path, include
from .views import HomeView, post_detail, PostCreateView, PostUpdateView, PostDeleteView, PostViewSet, FeaturePostViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'posts', PostViewSet)
router.register(r'featured-posts', FeaturePostViewSet, basename='feature-post')


urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('api/', include(router.urls)),
    # path('post/<int:pk>/', PostDetailView.as_view(), name="post_detail")
    path('post/<int:pk>/', post_detail, name="post_detail"),
    path('post/new/', PostCreateView.as_view(), name="post_create"),
    path('post/<int:pk>/edit/', PostUpdateView.as_view(), name="post_update"),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name="post_delete"),
]
