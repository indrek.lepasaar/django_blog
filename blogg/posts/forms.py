from django import forms
from .models import Comments, Posts


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comments
        fields = ['content']


class PostForm(forms.ModelForm):
    class Meta:
        model = Posts
        fields = ['title', 'author', 'content']
