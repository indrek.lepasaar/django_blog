from django.contrib import admin
from .models import Posts, Category, Comments, FeaturedPost

admin.site.register(Posts)
admin.site.register(Category)
admin.site.register(Comments)
admin.site.register(FeaturedPost)
