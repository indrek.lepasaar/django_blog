from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


# Create your models here.
class Posts(models.Model):
    title = models.CharField(max_length=100)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    date_posted = models.DateField(auto_now_add=True)
    categories = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="posts", null=True, blank=True)

    def __str__(self):
        return f"{self.title} - {self.author}"


class Comments(models.Model):
    post = models.ForeignKey(Posts, on_delete=models.CASCADE, related_name="comments")
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    date_posted = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"{self.author.username} - {self.post.title}"


class FeaturedPost(models.Model):
    post = models.OneToOneField(Posts, on_delete=models.CASCADE)
    priority = models.IntegerField(default=0)
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return f"Featured Post: {self.post.title}"
