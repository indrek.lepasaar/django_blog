from rest_framework import serializers
from .models import Posts, FeaturedPost, Comments


class CommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comments
        exclude = ['id', 'author', 'post']


class PostsSerializer(serializers.ModelSerializer):
    comments = CommentsSerializer(many=True, read_only=True)
    category_name = serializers.SerializerMethodField()
    author_name = serializers.SerializerMethodField()

    class Meta:
        model = Posts
        fields = ['title', 'author_name', 'content', 'date_posted', 'category_name', 'comments']  # You can specify fields that you don't want to publish

    def get_category_name(self, obj):
        return obj.categories.name if obj.categories else None

    def get_author_name(self, obj):
        return obj.author.username if obj.author else None


class FeaturedPostsSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeaturedPost
        fields = '__all__'  # You can specify fields that you don't want to publish
